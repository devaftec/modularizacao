<?php
namespace aftec\modularizacao;

use aftec\modularizacao\Console\Migrate;
use aftec\modularizacao\Console\MigrateBlock;
use aftec\modularizacao\Console\Request;
use Carbon\Laravel\ServiceProvider;
use aftec\modularizacao\Console\Controller;
use aftec\modularizacao\Console\Migration;
use aftec\modularizacao\Console\Model;
use aftec\modularizacao\Console\Modulo;

class LaravelModulosServiceProvider extends ServiceProvider
{
    public function register()
    {
        //
    }

    public function boot()
    {
        $this->commands([
            Modulo::class,
            Model::class,
            Controller::class,
            Migration::class,
            Request::class,
            Migrate::class
        ]);

        $this->registerRoutes();
        $this->registerMigrations();
    }

    protected function registerRoutes()
    {
        foreach (glob(base_path(). '/modules/*/Routes/*.php') as $router_files){
            $this->loadRoutesFrom($router_files);
        }
    }

    protected function registerMigrations()
    {
        $mainPath = database_path('Database/migrations');
        $directories = glob( base_path().'/modules/*/Database/migrations' , GLOB_ONLYDIR);
        $paths = array_merge([$mainPath], $directories);
        $this->loadMigrationsFrom($paths);
    }

}
