<?php

namespace aftec\modularizacao\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class Migration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'modulo:make-migration {modulo} {migration}  {--table=} {--create=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //Artisan::call("make:migration {$this->argument('migration')} --path=modules//{$this->argument('modulo')}//Database//migrations ".($this->option('table') ? "--table=".$this->option('table')  : "").($this->option('create') ? "--create=".$this->option('create')  : ""));
        //$this->info("Migrate {$this->argument('migration')} criado com sucesso");
        $tipo = "";
        $tabela = "";
        if ($this->option('table')){
            $tipo = "table";
            $tabela = $this->option('table');
        }else if ($this->option('create')){
            $tipo = "create";
            $tabela = $this->option('create');
        }

        Artisan::call("modulo:make-migrate {$this->argument('modulo')} {$this->argument('migration')} {$tipo} {$tabela}");
        $this->info("Migrate criado com sucesso");
        return 1;
    }


}
