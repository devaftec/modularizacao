<?php

namespace aftec\modularizacao\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class Migrate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'modulo:make-migrate {modulo} {migration} {tipo} {table} ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando para gerar novos migrates';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //$this->info($this->snakeToCamel($this->argument('descricao')));

        $file = "";

        if($this->argument('tipo') === 'table'){
            $file = $this->argument('migration');
        }else if($this->argument('tipo') === 'create'){
            $file = $this->argument('migration');
        }else{
            $this->info("Tipo informado({$this->argument('tipo')}), inválido!");
            return 0;
        }

        $path = "modules//{$this->argument('modulo')}//Database//migrations//";
        $class = $this->snakeToCamel($this->argument('migration'));
        $fileWithDate = now()->format('Y_m_d_His').'_'.$file;
        $this->createFile($path,$fileWithDate,$class,$this->argument('table'),$this->argument('tipo'));


        $this->info("Migrate {$fileWithDate}.php criado com sucesso");

        return 1;
    }

    private function createFile($path, $file, $class, $table, $tipo)
    {
        $contents = "<?php";
        $contents .= "\n";
        $contents .= "\n";
        $contents .= "use Illuminate\Database\Migrations\Migration;";
        $contents .= "\n";

        $contents .= "use Illuminate\Database\Schema\Blueprint;";
        $contents .= "\n";

        $contents .= "use Illuminate\Support\Facades\Schema;";
        $contents .= "\n";
        $contents .= "\n";
        $contents .= "class {$class} extends Migration"; //mudar nome da classe
        $contents .= "\n";
        $contents .= "{";
        $contents .= "\n";
        $contents .= "/**";
        $contents .= "\n";
        $contents .= "* Run the migrations.";
        $contents .= "\n";
        $contents .= "*";
        $contents .= "\n";
        $contents .= "* @return void";
        $contents .= "\n";
        $contents .= "*/";
        $contents .= "\n";
        $contents .= "  public function up()";
        $contents .= "\n";
        $contents .= "  {";
        $contents .= "\n";


        $contents .= '      Schema::'.$tipo.'("'.$table.'", function (Blueprint $table) {';


        $contents .= "\n";

        if ($tipo === 'create'){
            $contents .= '          $table->id();';
            $contents .= "\n";
            $contents .= '          $table->timestamps();';
        }


        $contents .= "\n";
        $contents .= "      });";
        $contents .= "\n";
        $contents .= "  }";
        $contents .= "\n";
        $contents .= "\n";
        $contents .= "/**";
        $contents .= "\n";
        $contents .= " * Reverse the migrations.";
        $contents .= "\n";
        $contents .= "*";
        $contents .= "\n";
        $contents .= "* @return void";
        $contents .= "\n";
        $contents .= "*/";
        $contents .= "\n";

        $contents .= "  public function down()";
        $contents .= "\n";
        $contents .= "  {";
        $contents .= "\n";
        $contents .= "      Schema::dropIfExists('{$table}');";
        $contents .= "\n";
        $contents .= "  }";
        $contents .= "\n";
        $contents .= "}";

        $myfile = fopen($path.$file.".php", "w") or die("Unable to open file!");
        $txt = $contents;
        fwrite($myfile, $txt);
        fclose($myfile);


    }

    function snakeToCamel ($str) {
        // Remove underscores, capitalize words, squash, lowercase first.
        return Str::ucfirst(lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $str)))));
    }

}