<?php

namespace aftec\modularizacao\Console;

use Illuminate\Console\Command;
use function PHPUnit\Framework\directoryExists;

class Controller extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'modulo:make-controller {modulo} {controller}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $controller = explode('//',$this->argument('controller'));
        $controller = end($controller);

        if (count(explode('//',$this->argument('controller'))) > 1)
        {
            $folders = explode('//', $this->argument('controller'));      // split string on :
            array_pop($folders);                   // get rid of last element
            $newFolder = implode('//', $folders);

            if (!is_dir("modules//{$this->argument('modulo')}//Http//Controllers//".$newFolder))
                mkdir("modules//{$this->argument('modulo')}//Http//Controllers//".$newFolder,0777,true);
        }

        $path = "modules//{$this->argument('modulo')}//Http//Controllers//{$this->argument('controller')}.php";


        $this->createFile($path,$this->argument('modulo'), $controller);
        $this->info('Controller '.$controller.' criado com sucesso em '.$path);

        return 0;
    }

    private function createFile($path,$modulo,$controller)
    {
        $contents = "<?php\n\n";
        $contents.= "namespace Modules\\".$modulo."\\Http\\Controllers;\n\n";
        $contents.= "use Illuminate\Http\Request;\n";
        $contents.= "use Illuminate\Routing\Controller;\n\n";
        $contents.= "class ".$controller." extends Controller\n";
        $contents.="{\n\n";
        $contents.="}\n";

        $myfile = fopen($path, "w") or die("Unable to open file!");
        $txt = $contents;
        fwrite($myfile, $txt);
        fclose($myfile);

    }
}
