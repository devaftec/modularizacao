<?php

namespace aftec\modularizacao\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class Request extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'modulo:make-request {modulo} {entidade} } {--c} {--u}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Criar os Requests da model';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */

    public function handle()
    {
        $modulo = $this->argument('modulo');
        $entidade = $this->argument('entidade');
        $path = "modules\\{$modulo}\\Http\\Requests\\{$entidade}\\";

        if ($this->option('c')){
            $this->createFile($path,$modulo,$entidade.'CreateRequest');
        }

        if ($this->option('u')){
            $this->createFile($path,$modulo,$entidade.'UpdateRequest');
        }

        if (!$this->option('u') && !$this->option('c')){
            $this->createFile($path,$modulo,$entidade.'Request');
        }


        $this->info("Request criado com sucesso em {$path}");
        return 0;
    }

    private function createFile($path, $modulo, $entidade): void
    {
        $txt = "<?php";
        $txt .= "\n";
        $txt .= "namespace Modules\\{$modulo}\\Http\\Requests\\{$this->argument('entidade')};\n";
        $txt .= "\n";
        $txt .= "use Illuminate\Foundation\Http\FormRequest;";
        $txt .= "\n";
        $txt .= "use Illuminate\Validation\Rule;";
        $txt .= "\n";
        $txt .= "class {$entidade} extends FormRequest";
        $txt .= "\n";
        $txt .= "{";
        $txt .= "\n";
        $txt .= "   /**";
        $txt .= "\n";
        $txt .= "   * Determine if the user is authorized to make this request.";
        $txt .= "\n";
        $txt .= "   *";
        $txt .= "\n";
        $txt .= "   * @return bool";
        $txt .= "\n";
        $txt .= "   */";
        $txt .= "\n";
        $txt .= "   public function authorize()";
        $txt .= "\n";
        $txt .= "   {";
        $txt .= "\n";
        $txt .= "       return true;";
        $txt .= "\n";
        $txt .= "   }";
        $txt .= "\n";
        $txt .= "\n";
        $txt .= "   /**";
        $txt .= "\n";
        $txt .= "   * Get the validation rules that apply to the request.";
        $txt .= "\n";
        $txt .= "   *";
        $txt .= "\n";
        $txt .= "   * @return array";
        $txt .= "\n";
        $txt .= "   */";
        $txt .= "\n";
        $txt .= "   public function rules()";
        $txt .= "   {";
        $txt .= "\n";
        $txt .= "       return [";
        $txt .= "\n";
        $txt .= "       ];";
        $txt .= "\n";
        $txt .= "   }";
        $txt .= "\n";
        $txt .= "}";


        if (!is_dir($path)){
            mkdir($path);
        }
        $myfile = fopen($path.$entidade.'.php', "w") or die("Unable to open file!");
        fwrite($myfile, $txt);
        fclose($myfile);








    }


}