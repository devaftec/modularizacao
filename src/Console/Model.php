<?php

namespace aftec\modularizacao\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class Model extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'modulo:make-model {modulo} {entidade} {--m} {--c} {--rc} {--ru}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cria a model em modules';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $model = explode('//',$this->argument('entidade'));
        $model = end($model);

        if (count(explode('//',$this->argument('entidade'))) > 1)
        {
            $folders = explode('//', $this->argument('entidade'));      // split string on :
            array_pop($folders);                   // get rid of last element
            $newFolder = implode('//', $folders);

            if (!is_dir("modules//{$this->argument('modulo')}//Models//".$newFolder))
                mkdir("modules//{$this->argument('modulo')}//Models//".$newFolder,0777,true);
        }

        $path = "modules//{$this->argument('modulo')}//Models//{$this->argument('entidade')}.php";
        $this->createFile($path,$this->argument('modulo'),$model);


        $saida = "Model {$model} criado com sucesso em {$path}";

        if ($this->option('m')){
            Artisan::call("modulo:make-migration {$this->argument('modulo')} create{$model}s_table");
            $saida .= "\nMigration criado com sucesso";
        }

        if ($this->option('c')){
            Artisan::call("modulo:make-controller {$this->argument('modulo')} {$this->argument('entidade')}Controller");
            $saida .= "\nController criado com sucesso";
        }

        if ($this->option('rc')){
            Artisan::call("make:request modules//{$this->argument('modulo')}//Requests//{$this->argument('entidade')}CreateRequest");
            $saida .= "\nRequest {$this->argument('entidade')}CreateRequest criado com sucesso";
        }

        if ($this->option('ru')){
            Artisan::call("make:request modules//{$this->argument('modulo')}//Requests//{$this->argument('entidade')}UpdateRequest");
            $saida .= "\nRequest {$this->argument('entidade')}UpdateRequest criado com sucesso";
        }

        $this->info($saida);
        return 0;
    }

    private function createFile($path,$modulo,$entidade)
    {

        $factory = "\n  protected static function newFactory()\n{\n     return \Modules\Cadastro\database\factories\CategoriaFactory::new();\n  }";
        $factory = "";
        $contents =
            "<?php\n\nnamespace Modules\\".$modulo."\\Models;\n\nuse Illuminate\Database\Eloquent\Model;\nuse Illuminate\Database\Eloquent\Factories\HasFactory;\n\nclass ".$entidade." extends Model\n{\n    use HasFactory;".$factory."\n}";


        $myfile = fopen($path, "w") or die("Unable to open file!");
        $txt = $contents;
        fwrite($myfile, $txt);
        fclose($myfile);



    }
}
